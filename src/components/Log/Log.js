import React from "react";
import withTable from "../TableHOC";

class Log extends React.Component {
  constructor(props) {
    super(props);

    this.header = "Log";

    this.customizedHeader = {
      display: "grid",
      gridTemplateColumns: "max-content auto",
      gridTemplateRows: "100%"
    };

    const requestType = [
      {
        className: "request-type-serve",
        value: "Phục vụ"
      },
      {
        className: "request-type-bill",
        value: "Tính tiền"
      },
      {
        className: "request-type-menu",
        value: "Gọi menu"
      }
    ];

    this.state = {
      tableData: {
        headerData: {
          time: "thời gian",
          requestId: "id yêu cầu",
          table: "bàn",
          staff: "nhân viên",
          requestType: "loại yêu cầu",
          waitTime: "thời gian chờ"
        },
        rowData: [
          {
            time: "20180604 11:00:00",
            requestId: "ID 001",
            table: "No. 01",
            staff: "Nguyen Van Anh",
            requestType: requestType[0],
            waitTime: "7 phút"
          },
          {
            time: "20180604 11:10:30",
            requestId: "ID 002",
            table: "No. 06",
            staff: "Nguyen Van Anh",
            requestType: requestType[2],
            waitTime: "10 phút 30 giây"
          },
          {
            time: "20180604 14:22:42",
            requestId: "ID 003",
            table: "No. 10",
            staff: "Nguyen Van Anh",
            requestType: requestType[1],
            waitTime: "11 phút 22 giây"
          },
          {
            time: "20180604 15:39:23",
            requestId: "ID 002",
            table: "No. 05",
            staff: "Nguyen Van Anh",
            requestType: requestType[1],
            waitTime: "5 phút 2 giây"
          }
        ]
      }
    };

    this.Header = props => {
      const data = props.headerData;
      return (
        <div className="table-header-wrapper">
          <div className="padding-table" />
          <div className="table-header log-header">
            <span>{data.time}</span>
            <span>{data.requestId}</span>
            <span>{data.table}</span>
            <span>{data.staff}</span>
            <span>{data.requestType}</span>
            <span>{data.waitTime}</span>
          </div>
        </div>
      );
    };

    this.Row = props => {
      const data = props.data;
      return (
        <div className="table-row-wrapper">
          <div className="padding-table">
            <div className="highlight" />
          </div>
          <div className="table-row log-row">
            <span>
              {data.time.split(/\s/g)[0]}&nbsp;
              <span style={{ color: "rgb(102, 183, 239)" }}>
                {data.time.split(/\s/g)[1]}
              </span>
            </span>
            <span>{data.requestId}</span>
            <span>{data.table}</span>
            <span>{data.staff}</span>
            <span>
              <span
                style={{
                  transform: "scale(1.5, 1.5)",
                  display: "inline-block"
                }}
                className={data.requestType.className}
              >
                &bull;&nbsp;
              </span>
              <span>{data.requestType.value}</span>
            </span>
            <span>{data.waitTime}</span>
          </div>
        </div>
      );
    };
  }

  render() {
    const TableComponent = withTable(
      this.Header,
      this.Row,
      this.state.tableData,
      "log"
    );
    return (
      <div className="content-wrapper">
        <div className="content">
          <div className="component">
            <div style={this.customizedHeader} className="component-header">
              <span>{this.header}</span>
              <div
                style={{
                  fontSize: "0.9rem",
                  justifySelf: "flex-end",
                  color: "rgb(167, 193, 218)",
                  cursor: "pointer"
                }}
              >
                <span>Sort by&nbsp;</span>
                <span style={{ color: "rgb(37, 157, 233)" }}>name&nbsp;</span>
                <i
                  style={{
                    transform: "rotate(-90deg)",
                    display: "inline-block",
                    fontSize: "1.2rem"
                  }}
                  className="icon ion-md-swap"
                />
              </div>
            </div>
            <div className="component-body">
              <TableComponent />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Log;
