import React from "react";

class TimeModal extends React.Component {
  constructor(props) {
    super(props);

    this.header = "Chọn giờ";
  }

  render() {
    return (
      <div className="modal time-modal">
        <div className="modal-header time-modal-header">
          <span>{this.header}</span>
          <i
            onClick={() => this.props.overlay(false)}
            className="icon ion-md-close"
          />
        </div>
        <div className="modal-body time-modal-body">
          <div className="time-inputs-wrapper">
            <div className="time-inputs">
              <div className="time-from">
                <span>Từ</span>
                <input type="text" />
                <i className="icon ion-md-time" />
              </div>
              <div className="time-to">
                <span>Đến</span>
                <input type="text" />
                <i className="icon ion-md-time" />
              </div>
            </div>
          </div>
        </div>
        <div className="modal-footer time-modal-footer">
          <div className="time-btn-remove center-middle">Xoá</div>
          <div className="time-btn-add center-middle">Chọn</div>
        </div>
      </div>
    );
  }
}

export default TimeModal;
