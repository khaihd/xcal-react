import React from "react";

class AddTableModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tables: Array.from(new Array(35), (val, index) => {
        return { num: index + 1, selectable: true, selected: false };
      })
    };
  }

  render() {
    return (
      <div className="modal add-table-modal-wrapper">
        <i className="icon ion-ios-arrow-dropleft-circle add-table-chevron" />
        <div className="add-table-modal">
          <div className="modal-header add-table-modal-header">
            <span>Chọn bàn</span>
            <i
              onClick={() => {
                this.handlerClearAll();
                this.props.overlay(false);
              }}
              className="icon ion-md-close"
            />
          </div>
          <div className="modal-body add-table-modal-body">
            <div className="add-table-list-table-wrapper">
              <div className="add-table-list-table">
                {this.state.tables.map(table => {
                  return (
                    <div
                      key={"add_table_modal_table_" + table.num}
                      onClick={() => this.handleSelectTable(table)}
                      className={
                        table.selectable
                          ? table.selected
                            ? " add-table-table-selected"
                            : " add-table-table-unselected"
                          : " add-table-table-unselectable"
                      }
                    >
                      {table.num}
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
          <div className="modal-footer add-table-modal-footer">
            <div
              onClick={() => this.handlerClearAll()}
              className="add-table-btn-remove center-middle"
            >
              Xoá
            </div>
            <div className="add-table-btn-add center-middle">Thêm</div>
          </div>
        </div>
        <i className="icon ion-ios-arrow-dropright-circle add-table-chevron" />
      </div>
    );
  }

  handleSelectTable(table) {
    if (table.selectable) {
      let modifiedTables = this.state.tables.map(t => {
        return Object.assign({}, t, {
          selected: t.num === table.num ? true : t.selected
        });
      });
      this.setState(prevState => {
        return {
          tables: modifiedTables
        };
      });
    }
  }

  handlerClearAll() {
    let modifiedTables = this.state.tables.map(t => {
      return Object.assign({}, t, { selected: false });
    });
    this.setState(prevState => {
      return {
        tables: modifiedTables
      };
    });
  }
}

export default AddTableModal;
