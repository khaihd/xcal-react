import React from "react";
import generalImg from "../../asset/imgs/general.jpg";
import API from "../../api";

class General extends React.Component {
  constructor(props) {
    super(props);

    this.header = "Cấu hình chung";

    const logLevel = [
      {
        level: 0,
        value: "Debug"
      },
      {
        level: 1,
        value: "Info"
      },
      {
        level: 2,
        value: "Warning"
      },
      {
        level: 3,
        value: "Error"
      },
      {
        level: 4,
        value: "Fatal"
      }
    ];

    this.state = {
      tableData: {
        rowData: [
          {
            name: "Tên quán",
            requestName: "shopName",
            value: "",
            displayMore: false
          },
          {
            name: "Địa chỉ",
            requestName: "address",
            value: "",
            displayMore: false
          },
          {
            name: "Thời gian mở cửa",
            requestName: "openTime",
            value: "",
            displayMore: false
          },
          {
            name: "Thông tin liên hệ",
            requestName: "contactInfo",
            value: "",
            displayMore: false
          },
          {
            name: "Thời gian chờ phục vụ tối đa",
            requestName: "maxWaitingTime",
            value: "",
            displayMore: false
          },
          {
            name: "Mức độ ghi log",
            requestName: "logLevel",
            value: "",
            logLevels: logLevel,
            currentLevel: logLevel[1],
            displayMore: false
          },
          {
            name: "Số yêu cầu nhận phục vụ tối đa",
            requestName: "maxRequestPerEmployee",
            value: 0,
            displayMore: false
          },
          {
            name: "Thời gian phục vụ cho 1 yêu cầu",
            requestName: "maxRequestTimeout",
            value: "",
            displayMore: false
          }
        ]
      },
      editObject: {
        id: -1,
        index: -1,
        value: ""
      }
    };
  }

  componentDidMount() {
    this.populateDataRow();
  }

  populateDataRow() {
    API.get("/api/shopinfo").then(res => {
      this.setState(prevState => {
        let modified = prevState.tableData.rowData.map(r => {
          return Object.assign({}, r, {
            value: res.data.result.data[0][r.requestName]
          });
        });
        return {
          tableData: {
            rowData: modified
          },
          editObject: {
            id: res.data.result.data[0].id
          }
        };
      });
    });
  }

  render() {
    return (
      <div style={{ paddingRight: "40%" }} className="content-wrapper">
        <div className="content">
          <div className="component">
            <div className="component-header">
              <span>{this.header}</span>
            </div>
            <div
              style={{ backgroundColor: "white" }}
              className="component-body"
            >
              <img className="general-img" src={generalImg} alt="" />
              <div className="general-table">
                {this.state.tableData.rowData.map((data, i) => {
                  return (
                    <div
                      key={this.header + "_row_" + i}
                      className="general-row"
                    >
                      <span>{data.name}</span>
                      {i === this.state.editObject.index ? (
                        <input
                          type="text"
                          defaultValue={data.value}
                          onChange={e => this.handleInputChange(e.target.value)}
                        />
                      ) : (
                        <span>{data.value}</span>
                      )}
                      <i
                        onClick={() => this.handleGeneralMoreClick(i)}
                        className="icon ion-md-more general-more"
                      />
                      <div
                        style={{ display: data.displayMore ? "grid" : "none" }}
                        className="list-tooltip-wrapper general-tooltip-wrapper"
                      >
                        <div className="list-tooltip general-tooltip">
                          {i === 5 ? (
                            data.logLevels.map(level => {
                              return (
                                <div
                                  key={this.header + "_constant_" + level.value}
                                  className="general-more-option"
                                >
                                  <span>{level.level}</span>
                                  <span style={{ paddingRight: "20px" }}>
                                    {level.value}
                                  </span>
                                </div>
                              );
                            })
                          ) : i === this.state.editObject.index ? (
                            <React.Fragment>
                              <div className="general-more-option">
                                <i className="icon ion-md-checkbox" />
                                <span
                                  onClick={() => this.edit()}
                                  style={{ marginRight: "60px" }}
                                >
                                  Lưu
                                </span>
                              </div>
                              <div className="general-more-option">
                                <i className="icon ion-md-close-circle" />
                                <span
                                  onClick={() => this.cancelEdit()}
                                  style={{ marginRight: "60px" }}
                                >
                                  Huỷ
                                </span>
                              </div>
                            </React.Fragment>
                          ) : (
                            <div className="general-more-option">
                              <i className="icon ion-md-create" />
                              <span
                                onClick={() => this.beginEdit(i)}
                                style={{ marginRight: "60px" }}
                              >
                                Sửa
                              </span>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  handleGeneralMoreClick(index) {
    let modifiedRows = this.state.tableData.rowData.map((data, i) => {
      return Object.assign({}, data, {
        displayMore: i === index ? !data.displayMore : false
      });
    });
    this.setState(prevState => {
      return {
        tableData: {
          rowData: modifiedRows
        }
      };
    });
  }

  handleInputChange(value) {
    this.setState(prevState => {
      return {
        editObject: {
          id: prevState.editObject.id,
          index: prevState.editObject.index,
          value: value
        }
      };
    });
  }

  beginEdit(i) {
    let modifiedRow = this.state.tableData.rowData.map(r => {
      return Object.assign({}, r, { displayMore: false });
    });
    this.setState(prevState => {
      return {
        tableData: {
          rowData: modifiedRow
        },
        editObject: {
          id: prevState.editObject.id,
          index: i,
          value: ""
        }
      };
    });
  }

  cancelEdit() {
    let modifiedRow = this.state.tableData.rowData.map(r => {
      return Object.assign({}, r, { displayMore: false });
    });
    this.setState({
      tableData: {
        rowData: modifiedRow
      },
      editObject: {
        id: -1,
        index: -1,
        value: ""
      }
    });
  }

  edit() {
    let requestObject = {};
    this.state.tableData.rowData.map((r, i) => {
      Object.assign(
        requestObject,
        {
          [r.requestName]:
            i === this.state.editObject.index
              ? this.state.editObject.value
              : r.value
        },
        {
          closeTime: 75602
        }
      );
    });
    API.put(`/api/shopinfo/${this.state.editObject.id}`, requestObject).then(
      res => {
        let modifiedRows = this.state.tableData.rowData.map((r, i) => {
          if (i === this.state.editObject.index) {
            return Object.assign({}, r, {
              value: this.state.editObject.value,
              displayMore: false
            });
          } else {
            return r;
          }
        });
        this.setState(prevState => {
          return {
            tableData: {
              rowData: modifiedRows
            },
            editObject: {
              id: -1,
              index: -1,
              value: ""
            }
          };
        });
      }
    );
  }
}

export default General;
