import React from "react";
import TabController from "../components/TabController";
import avatar from "../asset/imgs/panda_avatar.png";
import API from "../api";

class Dashboard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tabs: [
        {
          name: "Hoạt động",
          tabName: "activity",
          isActive: true
        },
        {
          name: "Phân việc",
          tabName: "job",
          isActive: false
        },
        {
          name: "Cấu hình chung",
          tabName: "general",
          isActive: false
        },
        {
          name: "Cấu hình thiết bị",
          tabName: "device",
          isActive: false
        },
        {
          name: "Log",
          tabName: "log",
          isActive: false
        },
        {
          name: "Biểu đồ",
          tabName: "chart",
          isActive: false
        }
      ]
    };
  }

  componentDidMount() {
    let token = sessionStorage.getItem("JWT");
    if (token) {
      Object.assign(API.defaults.headers, {
        Authorization: `Bearer ${token}`
      });
    } else {
      this.props.logout();
    }
  }

  render() {
    let activeTab = this.state.tabs.find(tab => tab.isActive);
    return (
      <React.Fragment>
        <div className="header">
          <div className="title center-middle">administrator</div>
          <div className="notification">
            <div className="bell center-middle">
              <i className="icon ion-md-notifications" />
            </div>
            <div className="num-of-noti" />
          </div>
        </div>
        <div className="body">
          <div className="side-menu">
            <div className="menu-wrapper">
              <div className="menu">
                {this.state.tabs.map(tab => {
                  return (
                    <DashboardItem
                      key={"dashboard_item_" + tab.tabName}
                      handleOnClick={() => this.handleOnClick(tab.tabName)}
                      isActive={tab.isActive}
                      name={tab.name}
                    />
                  );
                })}
              </div>
            </div>
            <div className="user-profile">
              <div className="avatar">
                <img src={avatar} alt="avatar" />
              </div>
              <div className="description">
                <span>Dao Tuan</span>
                <span>Tiếp tân 01</span>
              </div>
              <button onClick={this.props.logout} className="logout-btn">
                Đăng xuất
              </button>
            </div>
          </div>
          <TabController
            tabName={activeTab.tabName}
            overlay={this.props.overlay}
          />
        </div>
      </React.Fragment>
    );
  }

  shouldComponentUpdate(nextProps, nextState) {
    let oldTab = this.state.tabs.find(tab => tab.isActive);
    let newTab = nextState.tabs.find(tab => tab.isActive);
    return oldTab.tabName !== newTab.tabName;
  }

  handleOnClick(tabName) {
    this.setState(oldState => {
      let modifiedTabs = oldState.tabs.map(tab => {
        return Object.assign({}, tab, { isActive: tabName === tab.tabName });
      });
      return { tabs: modifiedTabs };
    });
  }
}

const DashboardItem = props => {
  return (
    <div
      onClick={props.handleOnClick}
      className={"menu-item" + (props.isActive ? " menu-item-active" : "")}
    >
      {props.name}
    </div>
  );
};

export default Dashboard;
