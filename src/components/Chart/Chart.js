import React from "react";

class Chart extends React.Component {
  constructor(props) {
    super(props);

    this.header = "Biểu đồ";

    this.state = {
      statisticsList: [
        {
          name: "Số lượt phục vụ",
          value: 70,
          backgroundColor: "rgb(110, 95, 159)"
        },
        {
          name: "Thời gian chờ trung bình",
          value: "5 phút/order",
          backgroundColor: "rgb(74, 173, 225)"
        }
      ]
    };
  }

  render() {
    return (
      <div className="content-wrapper">
        <div className="content">
          <div className="component">
            <div className="component-header">{this.header}</div>
            <div className="component-body">
              <div className="chart-grid">
                <div className="chart-grid-inputs">
                  <div className="chart-grid-from-to-date">
                    <input
                      type="text"
                      placeholder="Từ ngày - Đến ngày"
                      onFocus={() => this.props.overlay(true, "calendar")}
                    />
                    <i className="icon ion-md-calendar" />
                  </div>
                  <div className="chart-grid-in-range-time">
                    <input
                      onFocus={() => this.props.overlay(true, "time")}
                      type="text"
                      placeholder="Trong khoảng thời gian"
                    />
                    <i className="icon ion-md-time" />
                  </div>
                </div>
                <div className="chart-grid-statistics-list">
                  {this.state.statisticsList.map(stats => {
                    return (
                      <div
                        key={"chart_tab_" + stats.name + "_" + stats.value}
                        style={{ backgroundColor: stats.backgroundColor }}
                        className="chart-grid-statistics"
                      >
                        <span className="chart-grid-statistics-name">
                          {stats.name}
                        </span>
                        <span className="chart-grid-statistics-value">
                          {stats.value}
                        </span>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Chart;
