import React from "react";

class Device extends React.Component {
  constructor(props) {
    super(props);

    this.header = "Cấu hình thiết bị";

    this.customizedHeader = {
      display: "grid",
      gridTemplateColumns: "repeat(2, max-content) auto",
      gridTemplateRows: "max-content",
      columnGap: "10px"
    };

    const deviceType = [
      {
        num: 1,
        type: "Tại bàn"
      },
      {
        num: 2,
        type: "Nhận yêu cầu"
      }
    ];

    this.state = {
      tableData: {
        headerData: {
          idXcaller: "ID xCALLER",
          deviceType: "LOẠI THIẾT BỊ",
          tableId: "ID BÀN"
        },
        rowData: [
          {
            idXcaller: "mdas255fa1ssfwwtegdABCas1",
            deviceType: deviceType[0],
            tableId: "xxxxx",
            displayMore: false
          },
          {
            idXcaller: "hsfbkdjsb38ryi98u4e2hcsi271b",
            deviceType: deviceType[1],
            tableId: "xxxxx",
            displayMore: false
          }
        ]
      }
    };
  }

  render() {
    return (
      <div className="content-wrapper">
        <div className="content">
          <div className="component">
            <div style={this.customizedHeader} className="component-header">
              <span>{this.header}</span>
              <i
                style={{
                  color: "rgb(116, 203, 84)",
                  cursor: "pointer",
                  fontSize: "1.1rem"
                }}
                onClick={() => {
                  this.handleDeviceMoreClick(-1);
                  this.props.overlay(true, "addDevice");
                }}
                className="icon ion-md-add-circle"
              />
              <div className="device-refresh">
                <i className="icon ion-md-refresh" />
                <span>Refresh</span>
              </div>
            </div>
            <div className="component-body">
              <div className="device-table">
                <div className="device-header">
                  <span>{this.state.tableData.headerData.idXcaller}</span>
                  <span>{this.state.tableData.headerData.deviceType}</span>
                  <span>{this.state.tableData.headerData.tableId}</span>
                </div>
                <div className="device-body">
                  {this.state.tableData.rowData.map((data, i) => {
                    return (
                      <div
                        key={this.header + "_row_" + i}
                        className="device-row"
                      >
                        <span>{data.idXcaller}</span>
                        <span>
                          {data.deviceType.num + " - " + data.deviceType.type}
                        </span>
                        <span>{data.tableId}</span>
                        <i
                          onClick={() => this.handleDeviceMoreClick(i)}
                          className="icon ion-md-more device-more"
                        />
                        <div
                          style={{
                            display: data.displayMore ? "grid" : "none"
                          }}
                          className="list-tooltip-wrapper device-tooltip-wrapper"
                        >
                          <div className="list-tooltip device-tooltip">
                            <div className="device-more-option">
                              <i className="icon ion-md-create" />
                              <span>Sửa</span>
                            </div>
                            <div className="device-more-option">
                              <i className="icon ion-md-trash" />
                              <span>Xoá</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  handleDeviceMoreClick(index) {
    let modifiedRows = this.state.tableData.rowData.map((data, i) => {
      return Object.assign({}, data, {
        displayMore: index === i ? (data.displayMore ? false : true) : false
      });
    });
    this.setState(prevState => {
      return {
        tableData: {
          headerData: prevState.tableData.headerData,
          rowData: modifiedRows
        }
      };
    });
  }
}

export default Device;
