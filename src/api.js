import axios from "axios";

export default axios.create({
  baseURL: "http://ec2-54-179-186-159.ap-southeast-1.compute.amazonaws.com:3003"
});
