import React from "react";

class AddJobModal extends React.Component {
  render() {
    return (
      <div className="modal add-job-modal">
        <div className="modal-header add-job-modal-header">
          <span>Thêm phân việc</span>
          <i
            onClick={() => {
              this.props.overlay(false);
            }}
            className="icon ion-md-close"
          />
        </div>
        <div className="modal-body add-job-modal-body">
          <div className="add-job-list-input">
            <div className="add-job-input-item">
              <span className="add-job-input-name">ID ĐT</span>
              <input type="text" />
            </div>
            <div className="add-job-input-item">
              <span className="add-job-input-name">Mật khẩu</span>
              <input type="text" />
            </div>
            <div className="add-job-input-item">
              <span className="add-job-input-name">Tên nhân viên</span>
              <input type="text" />
            </div>
            <div className="add-job-input-item">
              <span className="add-job-input-name">Ca làm việc</span>
              <input type="text" />
            </div>
            <div className="add-job-input-item">
              <span className="add-job-input-name">Số điện thoại</span>
              <input type="text" />
            </div>
          </div>
        </div>
        <div className="modal-footer add-job-modal-footer">
          <div className="add-job-btn-add center-middle">Thêm</div>
        </div>
      </div>
    );
  }
}

export default AddJobModal;
