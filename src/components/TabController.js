import React from "react";
import Activity from "../components/Activity/Activity";
import Job from "../components/Job/Job";
import General from "../components/General/General";
import Device from "../components/Device/Device";
import Log from "../components/Log/Log";
import Chart from "../components/Chart/Chart";

class TabController extends React.Component {
  render() {
    switch (this.props.tabName) {
      case "activity":
        return <Activity {...this.props} />;
      case "job":
        return <Job {...this.props} />;
      case "general":
        return <General {...this.props} />;
      case "device":
        return <Device {...this.props} />;
      case "log":
        return <Log {...this.props} />;
      case "chart":
        return <Chart {...this.props} />;
      default:
        return null;
    }
  }
}

export default TabController;
