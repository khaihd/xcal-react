import React from "react";
import API from "../api";

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };

    this.login = this.login.bind(this);
  }

  render() {
    return (
      <div className="login-form">
        <div className="login-title center-middle">administrator</div>
        <div className="form">
          <div className="form-message center-middle">
            Đăng nhập vào tài khoản
          </div>
          <div className="form-inputs">
            <input
              onChange={e => this.setState({ username: e.target.value })}
              type="text"
              placeholder="Username"
            />
            <input
              onChange={e => this.setState({ password: e.target.value })}
              type="password"
              placeholder="Mật khẩu"
            />
            <button onClick={this.login}>Đăng nhập</button>
          </div>
          <div className="form-forget-password center-middle">
            <a>Quên mật khẩu</a>
          </div>
        </div>
      </div>
    );
  }

  login() {
    API.post(
      `/api/authorization?username=${this.state.username}&password=${
        this.state.password
      }`
    ).then(res => {
      sessionStorage.setItem("JWT", res.data.result.token);
      this.props.login(true);
    });
  }
}

export default LoginForm;
