import React, { Component } from "react";
import LoginForm from "./components/LoginForm";
import Dashboard from "./components/Dashboard";
import ModalController from "./modals/ModalController";
import "./index.css";
import "./dashboard.css";
import "./components/Activity/activity.css";
import "./components/Job/job.css";
import "./components/General/general.css";
import "./modals/AddTableModal/addTable.css";
import "./modals/AddJobModal/addJob.css";
import "./components/Device/device.css";
import "./modals/AddDeviceModal/addDevice.css";
import "./components/Log/log.css";
import "./components/Chart/Chart";
import "./components/Chart/chart.css";
import "./modals/CalendarModal/calendar.css";
import "./modals/TimeModal/time.css";

class App extends Component {
  constructor(props) {
    super(props);

    this.dashboardGrid = {
      gridTemplateColumns: "auto",
      gridTemplateRows: "60px auto"
    };

    this.loginGrid = {
      gridTemplateColumns: "35% auto 35%",
      gridTemplateRows: "20% auto 30%"
    };

    this.state = {
      authenticated: sessionStorage.getItem("JWT") !== null,
      wrapperGrid:
        sessionStorage.getItem("JWT") !== null
          ? this.dashboardGrid
          : this.loginGrid,
      overlay: false,
      currentModal: "addTable"
    };
    this.handleOverlay = this.handleOverlay.bind(this);
  }

  render() {
    return (
      <div
        className="wrapper"
        style={Object.assign({}, this.state.wrapperGrid, {
          backgroundColor: this.state.authenticated ? "transparent" : "#0079c4"
        })}
      >
        {this.state.authenticated ? (
          <Dashboard
            overlay={this.handleOverlay}
            logout={() => {
              sessionStorage.removeItem("JWT");
              this.authenticate(false);
            }}
          />
        ) : (
          <LoginForm
            login={authenticated => this.authenticate(authenticated)}
          />
        )}
        <div
          style={{ display: this.state.overlay ? "grid" : "none" }}
          className="screen-overlay"
        >
          <ModalController
            modalName={this.state.currentModal}
            overlay={this.handleOverlay}
          />
        </div>
      </div>
    );
  }

  handleOverlay(on, modal) {
    this.setState(prevState => {
      return {
        overlay: on,
        currentModal: modal
      };
    });
  }

  authenticate(authenticated) {
    this.setState(prevState => {
      return {
        authenticated: authenticated,
        wrapperGrid:
          authenticated || sessionStorage.getItem("JWT")
            ? this.dashboardGrid
            : this.loginGrid
      };
    });
  }
}

export default App;
