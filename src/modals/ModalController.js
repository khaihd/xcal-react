import React from "react";
import AddTableModal from "./AddTableModal/AddTableModal";
import AddJobModal from "./AddJobModal/AddJobModal";
import AddDeviceModal from "./AddDeviceModal/AddDeviceModal";
import CalendarModal from "./CalendarModal/CalendarModal";
import TimeModal from "./TimeModal/TimeModal";

class ModalController extends React.Component {
  render() {
    switch (this.props.modalName) {
      case "addTable":
        return <AddTableModal {...this.props} />;
      case "addJob":
        return <AddJobModal {...this.props} />;
      case "addDevice":
        return <AddDeviceModal {...this.props} />;
      case "calendar":
        return <CalendarModal {...this.props} />;
      case "time":
        return <TimeModal {...this.props} />;
      default:
        return null;
    }
  }
}

export default ModalController;
