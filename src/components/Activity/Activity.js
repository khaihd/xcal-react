import React from "react";
import withTable from "../TableHOC";

class Activity extends React.Component {
  constructor(props) {
    super(props);

    this.handleAcceptClick = this.handleAcceptClick.bind(this);
    this.handleListMoreClick = this.handleListMoreClick.bind(this);
    this.header = "Hoạt động";

    this.state = {
      tableData: {
        headerData: {
          table: "bàn",
          staff: "nhân viên",
          requestType: "loại yêu cầu",
          acceptTime: "thời gian nhận",
          requestStatus: "trạng thái yêu cầu",
          acceptRequest: "nhận yêu cầu",
          switchAcceptor: "đổi người nhận"
        },
        rowData: [
          {
            table: "No. 01",
            staff: "Nguyen Van Anh",
            requestType: {
              className: "request-type-serve",
              value: "Phục vụ"
            },
            acceptTime: "10:00 AM",
            requestStatus: {
              className: "not-accepted",
              value: "Chưa nhận yêu cầu"
            },
            acceptRequest: false,
            switchAcceptor: {
              value: "Danh sách",
              ids: ["ID 002", "ID 003", "ID 004"]
            },
            displayMore: false
          },
          {
            table: "No. 02",
            staff: "Nguyen Van Anh",
            requestType: {
              className: "request-type-serve",
              value: "Phục vụ"
            },
            acceptTime: "09:00 AM",
            acceptRequest: false,
            requestStatus: {
              className: "not-accepted",
              value: "Chưa nhận yêu cầu"
            },
            switchAcceptor: {
              value: "Danh sách",
              ids: ["ID 002", "ID 003", "ID 004"]
            },
            displayMore: false
          },
          {
            table: "No. 03",
            staff: "Nguyen Van Anh",
            requestType: {
              className: "request-type-bill",
              value: "Tính tiền"
            },
            acceptTime: "11:12 AM",
            requestStatus: {
              className: "accepted",
              value: "Đã nhận yêu cầu"
            },
            acceptRequest: true,
            switchAcceptor: {
              value: "Danh sách",
              ids: ["ID 002", "ID 003", "ID 004"]
            },
            displayMore: false
          },
          {
            table: "No. 04",
            staff: "Nguyen Van Anh",
            requestType: {
              className: "request-type-menu",
              value: "Gọi menu"
            },
            acceptTime: "09:34 AM",
            requestStatus: {
              className: "accepted",
              value: "Đã nhận yêu cầu"
            },
            acceptRequest: true,
            switchAcceptor: {
              value: "Danh sách",
              ids: ["ID 002", "ID 003", "ID 004"]
            },
            displayMore: false
          }
        ]
      }
    };

    this.Header = props => {
      const data = props.headerData;
      return (
        <div className="table-header-wrapper">
          <div className="padding-table" />
          <div className="table-header activity-header">
            <span>{data.table}</span>
            <span>{data.staff}</span>
            <span>{data.requestType}</span>
            <span>{data.acceptTime}</span>
            <span>{data.requestStatus}</span>
            <span>{data.acceptRequest}</span>
            <span>{data.switchAcceptor}</span>
          </div>
        </div>
      );
    };

    this.Row = props => {
      const { data, toggleAccept, listMoreOnClick, index } = props;
      return (
        <div className="table-row-wrapper">
          <div className="padding-table">
            <div className="highlight" />
          </div>
          <div className="table-row activity-row">
            <span>{data.table}</span>
            <span>{data.staff}</span>
            <span className={data.requestType.className}>
              {data.requestType.value}
            </span>
            <span>{data.acceptTime}</span>
            <div className={"request-status " + data.requestStatus.className}>
              <span>&bull;</span>
              <span>{data.requestStatus.value}</span>
            </div>
            <input
              onClick={() => toggleAccept(index)}
              defaultChecked={data.acceptRequest}
              type="checkbox"
            />
            <a onClick={() => listMoreOnClick(index)}>
              {data.switchAcceptor.value}
            </a>
            <div
              style={{ display: data.displayMore ? "grid" : "none" }}
              className="list-tooltip-wrapper activity-tooltip-wrapper"
            >
              <div className="list-tooltip activity-tooltip">
                {data.switchAcceptor.ids.map(id => {
                  return (
                    <div key={id} className="acceptor-id">
                      {id}
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      );
    };
  }

  render() {
    const TableComponent = withTable(
      this.Header,
      this.Row,
      this.state.tableData,
      "activity"
    );
    return (
      <div className="content-wrapper">
        <div className="content">
          <div className="component">
            <div className="component-header">{this.header}</div>
            <div className="component-body">
              <TableComponent
                toggleAccept={this.handleAcceptClick}
                listMoreOnClick={this.handleListMoreClick}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }

  handleAcceptClick(index) {
    let modifiedRows = this.state.tableData.rowData.map((data, i) => {
      return Object.assign({}, data, {
        acceptRequest: index === i ? !data.acceptRequest : data.acceptRequest
      });
    });
    this.setState(prevState => {
      return {
        tableData: {
          headerData: prevState.tableData.headerData,
          rowData: modifiedRows
        }
      };
    });
  }

  handleListMoreClick(index) {
    let modifiedRows = this.state.tableData.rowData.map((data, i) => {
      return Object.assign({}, data, {
        displayMore: i === index ? (data.displayMore ? false : true) : false
      });
    });
    this.setState(prevState => {
      return {
        tableData: {
          headerData: prevState.tableData.headerData,
          rowData: modifiedRows
        }
      };
    });
  }
}

export default Activity;
