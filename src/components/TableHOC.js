import React from "react";

export default function withTable(
  HeaderComponent,
  RowComponent,
  tableData,
  tableName
) {
  return class extends React.Component {
    render() {
      return (
        <div className="table">
          <HeaderComponent headerData={tableData.headerData} />
          <div className="table-body">
            {tableData.rowData.map((data, index) => {
              return (
                <RowComponent
                  key={tableName + "_row_" + index}
                  data={data}
                  index={index}
                  {...this.props}
                />
              );
            })}
          </div>
        </div>
      );
    }
  };
}
