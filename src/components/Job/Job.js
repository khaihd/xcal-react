import React from "react";
import withTable from "../TableHOC";

class Job extends React.Component {
  constructor(props) {
    super(props);

    this.handleJobMoreClick = this.handleJobMoreClick.bind(this);
    this.handleActiveClick = this.handleActiveClick.bind(this);
    this.header = "Phân việc";

    this.customizedHeader = {
      display: "grid",
      gridTemplateColumns: "repeat(2, max-content)",
      gridTemplateRows: "max-content",
      columnGap: "10px"
    };

    this.state = {
      tableData: {
        headerData: {
          id: "id đt",
          password: "mật khẩu",
          staff: "nhân viên",
          shift: "ca làm việc",
          phone: "số điện thoại",
          tables: "các số bàn phụ trách",
          active: "active"
        },
        rowData: [
          {
            id: "ID 001",
            password: "******",
            staff: "Nguyen Van Anh",
            shift: "Sáng/Chiều/Tối",
            phone: "0909123456",
            tables: [
              {
                num: 1,
                color: "rgb(246, 230, 82)",
                status: ">=2 waiters"
              },
              {
                num: 2,
                color: "rgb(246, 230, 82)",
                status: ">=2 waiters"
              },
              {
                num: 3,
                color: "rgb(191, 40, 40)",
                status: "no waiter"
              },
              {
                num: 4,
                color: "rgb(191, 40, 40)",
                status: "no waiter"
              }
            ],
            active: false,
            displayMore: false
          },
          {
            id: "ID 002",
            password: "******",
            staff: "Nguyen Van Anh",
            shift: "07:00 AM - 01:00 PM",
            phone: "0909123456",
            tables: [
              {
                num: 5,
                color: "transparent",
                status: "ok"
              },
              {
                num: 6,
                color: "rgb(246, 230, 82)",
                status: ">=2 waiters"
              },
              {
                num: 7,
                color: "transparent",
                status: "ok"
              },
              {
                num: 8,
                color: "transparent",
                status: "ok"
              }
            ],
            active: false,
            displayMore: false
          },
          {
            id: "ID 003",
            password: "******",
            staff: "Nguyen Van Anh",
            shift: "Chiều/Tối",
            phone: "0909123456",
            tables: [
              {
                num: 9,
                color: "transparent",
                status: "ok"
              },
              {
                num: 10,
                color: "transparent",
                status: "ok"
              },
              {
                num: 11,
                color: "transparent",
                status: "ok"
              },
              {
                num: 2,
                color: "rgb(246, 230, 82)",
                status: ">=2 waiters"
              }
            ],
            active: false,
            displayMore: false
          },
          {
            id: "ID 004",
            password: "******",
            staff: "Nguyen Van Anh",
            shift: "01:00 PM - 20:00 PM",
            phone: "0909123456",
            tables: [
              {
                num: 1,
                color: "rgb(246, 230, 82)",
                status: ">=2 waiters"
              },
              {
                num: 6,
                color: "rgb(246, 230, 82)",
                status: ">=2 waiters"
              },
              {
                num: 12,
                color: "transparent",
                status: "ok"
              }
            ],
            active: false,
            displayMore: false
          }
        ]
      }
    };

    this.Header = props => {
      const data = props.headerData;
      return (
        <div className="table-header-wrapper">
          <div className="padding-table" />
          <div className="table-header job-header">
            <span>{data.id}</span>
            <span
              style={{
                display: "grid",
                gridTemplateColumns: "repeat(2, max-content)",
                gridTemplateRows: "max-content",
                columnGap: "10px"
              }}
            >
              <span>{data.password}</span>
              <i style={{ cursor: "pointer" }} className="icon ion-md-eye" />
            </span>
            <span>{data.staff}</span>
            <span>{data.shift}</span>
            <span>{data.phone}</span>
            <span>{data.tables}</span>
            <span>{data.active}</span>
            <span />
          </div>
        </div>
      );
    };

    this.Row = props => {
      const { data, jobMoreOnClick, toggleActive, index } = props;
      return (
        <div className="table-row-wrapper">
          <div className="padding-table">
            <div className="highlight" />
          </div>
          <div className="table-row job-row">
            <span>{data.id}</span>
            <span>{data.password}</span>
            <span>{data.staff}</span>
            <span>{data.shift}</span>
            <span style={{ color: "rgb(137, 209, 113)" }}>{data.phone}</span>
            <div className="responsible-tables">
              {data.tables.map(table => {
                return (
                  <div
                    key={data.id + "_responsible_tables_" + table.num}
                    style={{ backgroundColor: table.color }}
                    className="responsible-table center-middle"
                  >
                    {table.num}
                  </div>
                );
              })}
            </div>
            <input
              onClick={() => toggleActive(index)}
              defaultChecked={data.active}
              type="checkbox"
            />
            <i
              onClick={() => jobMoreOnClick(index)}
              className="icon ion-md-more job-more"
            />
            <div
              style={{ display: data.displayMore ? "grid" : "none" }}
              className="list-tooltip-wrapper job-tooltip-wrapper"
            >
              <div className="list-tooltip job-tooltip">
                <div
                  onClick={() => {
                    this.handleJobMoreClick(-1);
                    this.props.overlay(true, "addTable");
                  }}
                  className="job-more-option"
                >
                  <i className="icon ion-md-add" />
                  <span>Thêm bàn</span>
                </div>
                <div className="job-more-option">
                  <i className="icon ion-md-create" />
                  <span>Sửa</span>
                </div>
                <div className="job-more-option">
                  <i className="icon ion-md-trash" />
                  <span>Xoá</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    };
  }

  render() {
    const TableComponent = withTable(
      this.Header,
      this.Row,
      this.state.tableData,
      "job"
    );
    return (
      <div style={{ paddingBottom: "0" }} className="content-wrapper">
        <div
          style={{
            height: "100%",
            gridTemplateRows: "max-content auto min-content"
          }}
          className="content"
        >
          <div className="component">
            <div style={this.customizedHeader} className="component-header">
              <span>{this.header}</span>
              <i
                onClick={() => {
                  this.handleJobMoreClick(-1);
                  this.props.overlay(true, "addJob");
                }}
                style={{
                  color: "rgb(116, 203, 84)",
                  cursor: "pointer",
                  fontSize: "1.1rem"
                }}
                className="icon ion-md-add-circle"
              />
            </div>
            <div className="component-body">
              <TableComponent
                jobMoreOnClick={this.handleJobMoreClick}
                toggleActive={this.handleActiveClick}
              />
            </div>
          </div>
          <div
            style={{ gridColumn: "1 / span 1", gridRow: "3 / span 1" }}
            className="job-legends"
          >
            <div className="job-legend">
              <div style={{ backgroundColor: "rgb(246, 230, 82)" }} />
              <span>: Nhiều hơn 2 người phục vụ</span>
            </div>
            <div className="job-legend">
              <div style={{ backgroundColor: "rgb(191, 40, 40)" }} />
              <span>: Chưa có người phục vụ</span>
            </div>
          </div>
        </div>
      </div>
    );
  }

  handleActiveClick(index) {
    let modifiedRows = this.state.tableData.rowData.map((data, i) => {
      return Object.assign({}, data, {
        active: index === i ? !data.active : data.active
      });
    });
    this.setState(prevState => {
      return {
        tableData: {
          headerData: prevState.tableData.headerData,
          rowData: modifiedRows
        }
      };
    });
  }

  handleJobMoreClick(index) {
    let modifiedRows = this.state.tableData.rowData.map((data, i) => {
      return Object.assign({}, data, {
        displayMore: index === i ? (data.displayMore ? false : true) : false
      });
    });
    this.setState(prevState => {
      return {
        tableData: {
          headerData: prevState.tableData.headerData,
          rowData: modifiedRows
        }
      };
    });
  }
}

export default Job;
