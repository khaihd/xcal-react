import React from "react";

class CalendarModal extends React.Component {
  constructor(props) {
    super(props);

    this.currentDate = new Date();

    this.blockSize = 35;

    this.startOfYear = new Date(2018, 0, 1);

    this.length = 365 * 1;

    this.endOfYear = new Date(this.startOfYear);

    this.endOfYear.setDate(this.endOfYear.getDate() + this.length);

    this.datesInfo = this.initDateInfo(
      this.blockSize,
      this.startOfYear,
      this.length
    );

    this.state = {
      currentDate: this.currentDate,
      startDate: this.currentDate.toLocaleDateString("vi-vn", {
        weekday: "short",
        day: "2-digit",
        month: "2-digit",
        year: "numeric"
      }),
      endDate: ""
    };
  }

  render() {
    return (
      <div className="modal calendar-modal">
        <div className="modal-header calendar-modal-header">
          <span>{this.state.startDate + " - " + this.state.endDate}</span>
          <i
            onClick={() => this.props.overlay(false)}
            className="icon ion-md-close"
          />
        </div>
        <div className="modal-body calendar-modal-body">
          <div className="calendar-date-wrapper">
            <div className="calendar-date">
              <div className="calendar-date-header">
                <div className="calendar-date-header-months">
                  <span>
                    {this.state.currentDate.toLocaleString("en-us", {
                      month: "long"
                    }) +
                      " " +
                      this.state.currentDate.getFullYear()}
                  </span>
                  <i
                    onClick={() => this.changeMonth(-1)}
                    className="icon ion-ios-arrow-dropleft"
                  />
                  <i
                    onClick={() => this.changeMonth(1)}
                    className="icon ion-ios-arrow-dropright"
                  />
                </div>
                <div className="calendar-date-header-days">
                  <span>S</span>
                  <span>M</span>
                  <span>T</span>
                  <span>W</span>
                  <span>T</span>
                  <span>F</span>
                  <span>S</span>
                </div>
              </div>
              <div className="calendar-date-body">
                {this.getBlockByYearMonthDate(
                  this.datesInfo,
                  this.state.currentDate.getFullYear(),
                  this.state.currentDate.getMonth(),
                  this.state.currentDate.getDate()
                ).map(date => {
                  return (
                    <span
                      key={"calendar_modal_" + date.fullDate}
                      style={{
                        color:
                          date.month === this.state.currentDate.getMonth()
                            ? "rgb(141, 149, 171)"
                            : "rgb(230, 232, 235)"
                      }}
                    >
                      {date.date}
                    </span>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
        <div className="modal-footer calendar-modal-footer">
          <div className="calendar-btn-remove center-middle">Xoá</div>
          <div className="calendar-btn-add center-middle">Chọn</div>
        </div>
      </div>
    );
  }

  initDateInfo(size, startOfYear, length) {
    let tempDatesInfo = [];
    let startLimit = startOfYear;
    if (startOfYear.getDay() !== 0) {
      let previousDate;
      for (let i = 0; i < 7; i++) {
        previousDate = new Date(startLimit);
        previousDate.setDate(startLimit.getDate() - i);
        if (previousDate.getDay() === 0) {
          length += i; // to compensate the offset of previous start of year, otherwise it would be exactly 1 cycle (e.g 1 year) after start of year
          break;
        }
      }
      startLimit = previousDate;
    }
    for (let i = 0; i < length; i++) {
      let d = new Date(startLimit);
      d.setDate(startLimit.getDate() + i);
      tempDatesInfo.push({
        year: d.getFullYear(),
        month: d.getMonth(),
        date: d.getDate(),
        day: d.getDay(),
        fullDate: d.toLocaleDateString("vi-vn", {
          weekday: "long",
          day: "2-digit",
          month: "2-digit",
          year: "numeric"
        })
      });
    }

    let block = [];
    let result = [];

    tempDatesInfo.reduce((acc, cur, curIndex) => {
      acc.push(cur);
      if (acc.length === size) {
        result.push(acc);
        return [];
      } else if (curIndex === tempDatesInfo.length - 1) {
        result.push(acc);
        return [];
      } else {
        return acc;
      }
    }, block);

    return result;
  }

  getBlockByYearMonthDate(datesInfo, year, month, date) {
    return datesInfo.find(block => {
      return block.find(d => {
        return d.month === month && d.year === year && d.date === date;
      });
    });
  }

  changeMonth(step) {
    let newDate = new Date(this.state.currentDate);
    newDate.setMonth(newDate.getMonth() + step);

    if (newDate.getFullYear() == this.endOfYear.getFullYear()) {
      newDate.setFullYear(this.endOfYear.getFullYear() - 1);
      newDate.setMonth(11);
      newDate.setDate(31);
      // to handle when user navigate to the last day of end of year
      // because there will be some days left at the final block of the dates info
    } else if (newDate.getFullYear() == this.startOfYear.getFullYear() - 1) {
      newDate.setFullYear(this.startOfYear.getFullYear());
      newDate.setMonth(0);
      newDate.setDate(1);
      // to handle when user navigate to the first day of start of year
    } else if (
      newDate.getMonth() == this.currentDate.getMonth() &&
      newDate.getFullYear() == this.currentDate.getFullYear()
    ) {
      newDate = new Date();
    } else {
      newDate.setDate(1);
    }

    this.setState({ currentDate: newDate });
  }
}

export default CalendarModal;
