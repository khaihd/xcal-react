import React from "react";

class AddDeviceModal extends React.Component {
  render() {
    return (
      <div className="modal add-device-modal">
        <div className="modal-header add-device-modal-header">
          <span>Thêm cấu hình</span>
          <i
            onClick={() => {
              this.props.overlay(false);
            }}
            className="icon ion-md-close"
          />
        </div>
        <div className="modal-body add-device-modal-body">
          <div className="add-device-list-input">
            <div className="add-device-input-item">
              <span className="add-device-input-name">ID xCALLER</span>
              <input type="text" />
            </div>
            <div className="add-device-input-item">
              <span className="add-device-input-name">Loại thiết bị</span>
              <input type="text" />
            </div>
            <div className="add-device-input-item">
              <span className="add-device-input-name">ID Bàn</span>
              <input type="text" />
            </div>
          </div>
        </div>
        <div className="modal-footer add-device-modal-footer">
          <div className="add-device-btn-add center-middle">Thêm</div>
        </div>
      </div>
    );
  }
}

export default AddDeviceModal;
